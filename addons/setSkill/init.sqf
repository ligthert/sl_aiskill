diag_log "== Starting AI Skill modifier ==";

private ["_counter","_starttime","_endtime"];

// SL_AI_DEBUG
if (isNil "SL_AI_DEBUG" ) then { SL_AI_DEBUG = 0; };

// SL_AI_SLEEP
if (isNil "SL_AI_SLEEP" ) then { SL_AI_SLEEP = 60; };

// Side of the units to "enhance"
if (isNil "SL_AI_SIDE" ) then { SL_AI_SIDE = ["EAST","GUER"]; };

// Ref: https://community.bistudio.com/wiki/setUnitAbility
if (isNil "SL_AI_ABILITY" ) then { SL_AI_ABILITY = 0.8; };

// Ref: https://community.bistudio.com/wiki/AI_Sub-skills
if (isNil "SL_AI_AIMACC" ) then { SL_AI_AIMACC = ( 0.2 + (random 0.1) ); };
if (isNil "SL_AI_AIMSHA" ) then { SL_AI_AIMSHA = 0.15; };
if (isNil "SL_AI_AIMSPE" ) then { SL_AI_AIMSPE = 0.25; };
if (isNil "SL_AI_SPOTDIST" ) then { SL_AI_SPOTDIST = 0.85; };
if (isNil "SL_AI_SPOTTIME" ) then { SL_AI_SPOTTIME = 0.7; };
if (isNil "SL_AI_COURAGE" ) then { SL_AI_COURAGE = 0.35; };
if (isNil "SL_AI_RELOADS" ) then { SL_AI_RELOADS = 0.8; };
if (isNil "SL_AI_CMNDS" ) then { SL_AI_CMNDS = 0.75; };
if (isNil "SL_AI_GENERAL" ) then { SL_AI_GENERAL = 0.75; };

// SL_AI_FLEEING -- https://community.bistudio.com/wiki/allowFleeing
if (isNil "SL_AI_FLEEING") then { SL_AI_FLEEING = 0; };

// SL_AI_NOTIS - https://community.bistudio.com/wiki/disableTIEquipment
if (isNil "SL_AI_NOTIS") then { SL_AI_NOTIS = 1; };

if (isNil "SL_AI_STAMINA") then { SL_AI_STAMINA = 1 };

while {true} do {

	_counter = 0;

	{
		if (local _x AND _x != player AND str side _x in SL_AI_SIDE ) then {
			_x setUnitAbility SL_AI_ABILITY;

			_x setSkill ["aimingAccuracy", SL_AI_AIMACC];
			_x setSkill ["aimingShake", SL_AI_AIMSHA];
			_x setSkill ["aimingSpeed", SL_AI_AIMSPE];
			_x setSkill ["spotDistance", SL_AI_SPOTDIST];
			_x setSkill ["spotTime", SL_AI_SPOTTIME];
			_x setSkill ["courage", SL_AI_COURAGE];
			_x setSkill ["reloadSpeed", SL_AI_RELOADS];
			_x setSkill ["commanding", SL_AI_CMNDS];
			_x setSkill ["general", SL_AI_GENERAL];

			_x allowFleeing SL_AI_FLEEING;
			if ( SL_AI_NOTIS == 1 ) then { (vehicle _x) disableTIEquipment true; } else { (vehicle _x) disableTIEquipment false; };
			if ( SL_AI_STAMINA == 1 ) then { _x enableStamina false; _x enableFatigue false; } else { _x enableStamina true; _x enableFatigue true; };

			if ( SL_AI_DEBUG == 1 ) then { _counter = _counter + 1; };
		};

	} forEach allUnits;

	if ( SL_AI_DEBUG == 1 ) then { diag_log format["AI Skill: %1 AI modified", _counter]; };

	sleep SL_AI_SLEEP;
};
