class CfgPatches
{
	class SL_AISkill
	{
		units[] = {};
		weapons[] = {};
		requiredAddons[] = {"cba_events"};
		author[]= {"Sacha Ligthert"};
	};
};

class Extended_PostInit_EventHandlers
{
	SL_AISkill_postinit="[] execVM '\setskill\init.sqf'";
};
